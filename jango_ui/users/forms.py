from django.forms import CharField, EmailField
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class SignUpForm(UserCreationForm):
    email = EmailField(max_length=255)
    first_name = CharField(max_length=20)
    last_name = CharField(max_length=20)

    class Meta:
        model = User
        fields = ["username",
                  "first_name",
                  "last_name",
                  "email"]
