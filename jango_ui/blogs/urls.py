from django.urls import path
from . import views


app_name = 'blogs'


urlpatterns = [
    path('', views.home_page, name='home_page'),
    path('all', views.BlogsView.as_view(), name='blogs_list'),
    path('blog/<int:id>/', views.blog_view, name='blog'),
    path('Categories/<str:slug>/', views.category_view, name='category'),
    path('home_page', views.HomePageView.as_view(), name='home_page'),
    path('author/<str:author>/', views.author_viev, name='author_blogs'),
    path('user_home/', views.UserHomeView.as_view(), name='user_home'),
    path('add/', views.AddBlogFormView.as_view(), name='add_blog'),
    path('update/<str:slug>/', views.UpdateBlogFormView.as_view(), name='update_blog'),
    path('add_category/', views.AddCategoryFormView.as_view(), name='add_category')
]

