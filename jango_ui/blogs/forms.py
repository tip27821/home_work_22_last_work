from django import forms
from . import models
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class BlogsForm(forms.ModelForm):
    class Meta:
        model = models.Blogs
        fields = ['name', 'category', 'text', 'image']


class RegisterForm(UserCreationForm):
    email = forms.EmailField(label="Email")
    fullname = forms.CharField(label="Full name")

    class Meta:
        model = User
        fields = ["username", "fullname", "email"]


class CategoryForm(forms.ModelForm):
    class Meta:
        model = models.Category
        fields = ['category']
