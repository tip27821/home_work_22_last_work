from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.shortcuts import render
from django.template.context_processors import request
from .models import Blogs, Category
from django.views.generic import ListView, DetailView, UpdateView, CreateView


def home_page(request):
    return render(request, 'blogs/home_page.html')


class BlogsView(ListView):
    model = Blogs
    template_name = 'blogs/all_blogs.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['url'] = "http://127.0.0.1:8123/api/all/"
        context['Name'] = "Все записи"
        return context


def blog_view(request, id):
    return render(request, 'blogs/blog.html',
                  context={'url': f"http://127.0.0.1:8123/api/blog/{id}/",
                           'Name': f"Blog-{id}"
                           })


def category_view(request, slug):
        return render(request, 'blogs/all_blogs.html',
              context={'url': f"http://127.0.0.1:8123/api/category/{slug}/",
                       'Name': slug
                      })


def author_viev(request, author):
        return render(request, 'blogs/all_blogs.html',
              context={'url': f"http://127.0.0.1:8123/api/user/{author}/",
                       'Name': author
                       })


class HomePageView(ListView):
    model = Blogs
    context_object_name = 'blogs'
    template_name = 'blogs/home_page.html'


class AddBlogFormView(LoginRequiredMixin, CreateView):
    template_name = 'blogs/add_blog.html'
    model = Blogs
    fields = ['name', 'category', 'text', 'image']

    def form_valid(self, form):
        new_blog = form.save(commit=False)
        new_blog.author = self.request.user
        return super().form_valid(form)


class UpdateBlogFormView(LoginRequiredMixin, UpdateView):
        template_name = 'blogs/update_blog.html'
        model = Blogs
        fields = ['name', 'category', 'text', 'image']

        def form_valid(self, form):
            if self.request.user.id == self.object.author.id:
                return super().form_valid(form)
            else:
                author = self.object.author
                return render(self.request, 'blogs/error.html', context={'author': author})


class UserHomeView(LoginRequiredMixin, ListView):
    template_name = 'blogs/user_home.html'
    model = Blogs

    def form_valid(self, form):
        new_blog = form.save(commit=False)
        new_blog.author = self.request.user
        return super().request


class AddCategoryFormView(LoginRequiredMixin, CreateView):
    template_name = 'blogs/add_category.html'
    model = Category
    fields = ['category']

    def form_valid(self, form):
        new_blog = form.save(commit=False)
        new_blog.author = self.request.user
        return super().form_valid(form)
