from django.contrib.auth.models import User
from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView
from .serializers import AllSerializer, PostSerializer
from .models import Blogs, Category
import random
import string


def rand_word():
    rand_words = ["".join(random.choice(string.ascii_letters)
                  for j in range(random.randint(3, 15))) for i in range(random.randint(3, 10))]
    return random.choice(rand_words)


class GetAllDataApiView(ListAPIView):
    serializer_class = AllSerializer
    queryset = Blogs.objects.all()


class GetCatDataApiView(ListAPIView):
    serializer_class = AllSerializer
    queryset = Blogs.objects.all()

    def get_queryset(self):
        category = self.kwargs['category']
        return Blogs.objects.filter(category__category=category)


class GetUserDataApiView(ListAPIView):
    serializer_class = AllSerializer

    def get_queryset(self):
        username = self.kwargs['username']
        return Blogs.objects.filter(author__username=username)


class GetBlogDataApiView(RetrieveAPIView):
    serializer_class = AllSerializer

    def get_queryset(self):
        pk = self.kwargs['pk']
        return Blogs.objects.filter(pk=pk)


class AddBlogDataApiView(CreateAPIView):
    queryset = Blogs.objects.all()
    serializer_class = PostSerializer

    def perform_create(self, serializer):
        if self.request.method == 'POST':

            if len(list(User.objects.all())) == 2:
                num = 1
                while num < 10:
                    lala = User(username=rand_word())
                    lala.save()
                    num += 1

            if len(list(Category.objects.all())) == 2:
                num = 1
                while num < 20:
                    rand_cat = Category(category=rand_word())
                    rand_cat.save()
                    num+=1

            serializer.save(name=rand_word(),
                            author=random.choice(User.objects.all()),
                            category=random.choice(Category.objects.all()),
                            text=f'{rand_word()}, {rand_word()}, {rand_word()}')
