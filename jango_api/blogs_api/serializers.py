from django.contrib.auth.models import User
from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import SlugRelatedField
from .models import Blogs, Category


class AllSerializer(ModelSerializer):
    category = SlugRelatedField(queryset=Category.objects.all(), slug_field='category')
    author = SlugRelatedField(queryset=User.objects.all(), slug_field='username')

    class Meta:
        model = Blogs
        fields = ['id', 'name', 'slug', 'category', 'text', 'author', 'image', 'date']


class PostSerializer(ModelSerializer):
    class Meta:
        model = Blogs
        fields = ['name', 'category', 'text', 'author']
