from django.urls import path

from .views import GetAllDataApiView, GetCatDataApiView, GetUserDataApiView, GetBlogDataApiView, AddBlogDataApiView

app_name = 'blogs_api'

urlpatterns = [
    path('all/', GetAllDataApiView.as_view()),
    path('category/<str:category>/', GetCatDataApiView.as_view()),
    path('user/<str:username>/', GetUserDataApiView.as_view()),
    path('blog/<int:pk>/', GetBlogDataApiView.as_view()),
    path('add/', AddBlogDataApiView.as_view()),
]
